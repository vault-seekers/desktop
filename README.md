# Vault Seekers
**Obsolete; rewritten and moved to https://gitlab.com/magicalbits/vault-seekers/client**

Vault Seekers is a card game for 2-5 players.
Each player tries to get as many resources as they can to win the game.

