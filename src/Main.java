import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.Color;

public class Main {
    public static Board boardBlue;
    public static Board boardRed;
    public static Board boardEndTurn;
    public static final JFrame mainFrame = new JFrame();

    public static void main(String[] args) {
        SwingUtilities.invokeLater(Main::createAndShowGUI);
    }

    // TODO (maybe): add an overlay with a JLabel containing text "{color} player's turn" and a button to
    //  load the {color} player's board (to prevent the other player seeing his opponent's board)
    public static void swapBoards(String currentBoardColor) {
        switch (currentBoardColor) {
            case "blue":
                boardBlue.hide();

                mainFrame.setContentPane(boardRed.CONTENT_PANE);
                mainFrame.validate();
                mainFrame.repaint();

                boardRed.show();
                break;
            case "red":
                boardRed.hide();

                mainFrame.setContentPane(boardEndTurn.CONTENT_PANE);
                Board.revealCards();
                mainFrame.validate();
                mainFrame.repaint();

                boardEndTurn.show();
                Board.resolveConflict();
                break;
            case "noColor":
                boardEndTurn.hide();

                mainFrame.setContentPane(boardBlue.CONTENT_PANE);

                boardBlue.show();
                boardBlue.placeSwapBoardsButton();

                boardBlue.placePlayingCards();
                boardRed.placePlayingCards();

                mainFrame.validate();
                mainFrame.repaint();
                break;
        }
    }

    public static void swapExtraBoss() {
        if (boardRed.hasExtraBoss()) {
            boardRed.removeExtraBoss();
            boardBlue.assignExtraBoss();
        } else {
            boardBlue.removeExtraBoss();
            boardRed.assignExtraBoss();
        }
    }

    private static void createAndShowGUI() {
        System.out.printf("isEDT(): %b%n", SwingUtilities.isEventDispatchThread());
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setUndecorated(true);
//        mainFrame.setBackground(new Color(0, 0, 0)); // dark mode (background)
        mainFrame.setBackground(new Color(238, 238, 238));

        boardEndTurn = new Board();

        boardRed = new Board("red");

        // TODO (maybe): randomly pick a starting player
        // Blue player always starts
        boardBlue = new Board("blue");

        // Borderless window
        mainFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);

        Board.initVaultCards();

//        String[][] testArray = new String[40][2];
//        for (int i = 0; i < testArray.length; i++) {
//            testArray[i][0] = Integer.toString(i);
//            testArray[i][1] = Integer.toString(100 - i);
//        }
//        Board.shuffle2DArray(testArray);

        Board.shuffleVaultCards();
        Board.placeVaultCards();
        Board.giveStarterGems();

        // TODO: assign the optional green boss to one of the two players, randomly
        boardBlue.assignExtraBoss();
        boardBlue.show();
    }
}
