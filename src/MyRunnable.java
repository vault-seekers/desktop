import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import java.util.Arrays;

public class MyRunnable implements Runnable {
    @Override
    public void run() {
        resolveConflict();
    }

    // Could be used for receiving data from event dispatch thread
    public MyRunnable() {
    }

    // Current status: vault cards get switched seamlessly without any flashing sequence
    // TODO: make the to-be-removed vault card flash three times (500ms off, 500ms on; do that 2 times,
    //  then hide it until a new card shows using the same but reversed sequence)
    private void resolveConflict() {
        for (int i = 0; i < Board.vaultCardSlots.length; i++) {
            int finalI = i;

            System.out.printf("[for; i == %d]%n", i);
            System.out.println(Arrays.toString(Board.attackCardCount));
            System.out.println(Arrays.toString(Board.defenseCardCount));

            if (Board.attackCardCount[i] + Board.defenseCardCount[i] == 0) {
                // Remove vault card, wait for some time, place another vault card if there's one available

                System.out.printf("(vaultCardSlots[%d]) removing (800ms delay) ...%n", i);
                try {Thread.sleep(800);} catch (Exception e) {e.printStackTrace();}

                SwingUtilities.invokeLater(() -> {
                    JLabel jLabel = (JLabel) Main.mainFrame.getContentPane().getComponent(finalI + 69);
                    jLabel.setIcon(null);

                    Board.vaultCardSlots[finalI] = null;
                    System.out.printf("(vaultCardSlots[%d]) removed!%n", finalI);

                    Board.placeVaultCards();
                    System.out.printf("(vaultCardSlots[%d]) placed!%n", finalI);
                });
            } else if (Board.defenseCardCount[i] > 0) {
                // Hide playing cards (one by one, delay 1s between each disappearing), keep vault card

                try {Thread.sleep(1000);} catch (Exception e) {e.printStackTrace();}
                System.out.printf("removing blue||red playing card %d (1000ms delay)%n", i);
                SwingUtilities.invokeLater(() -> {
                    for (int j = 0; j < 2; j++) {
                        // Hide attack cards, if any
                        JLabel jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.AttackCardSlot.A.getTopPos() + finalI + j * 26);
                        if (jLabel.getIcon() != null) {
                            jLabel.setIcon(null);
                            jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.AttackCardSlot.A.getBottomPos() + finalI + j * 26);
                            jLabel.setIcon(null);
                        }

                        // Hide defense cards, if any
                        jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.DefenseCardSlot.A.getTopPos() + finalI - j * 26);
                        if (jLabel.getIcon() != null) {
                            jLabel.setIcon(null);
                            jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.DefenseCardSlot.A.getBottomPos() + finalI - j * 26);
                            jLabel.setIcon(null);
                        }
                    }
                });
            } else if (Board.attackCardCount[i] > 1) {
                SwingUtilities.invokeLater(() -> {
                    // maybe merge the two TODOs below into one (the method could check if the HLC is already placed and
                    //  if it is, it could just increment each attacker's number of HLCs
                    // TODO: give one hidden loot card to each attacker
                    // TODO: add a condition to check if a hidden loot card is already placed
                    Board.placeHiddenLootCard();
                });
            } else if (Board.attackCardCount[i] == 1) {
                SwingUtilities.invokeLater(() -> {
                    // TODO: give loot to the attacker
                    // TODO: replace vault card, if there's > 1 available
                });
            }
            Board.resetPlayingCardsPos(i);
        }
        Board.attackCardCount = new int[Board.attackCardCount.length];
        Board.defenseCardCount = new int[Board.defenseCardCount.length];
        try {Thread.sleep(1000);} catch (Exception e) {e.printStackTrace();}
        SwingUtilities.invokeLater(() -> Main.boardEndTurn.placeNextRoundButton());
    }
}
