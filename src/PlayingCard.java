import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Container;
import java.awt.event.MouseAdapter;

public class PlayingCard extends Card {
    private final Board BOARD;
    private final String COLOR;
    private final char LETTER;
    private final ImageIcon IMAGE_ICON_TOP;
    private final ImageIcon IMAGE_ICON_BOTTOM;
    private final int DEFAULT_TOP_POS;
    private final int DEFAULT_BOTTOM_POS;
    private int currentTopPos;
    private int currentBottomPos;
    private String currentStatus = "hand";
    protected MouseAdapter mouseAdapterHand;
    protected MouseAdapter mouseAdapterAttDef;

    // TODO (maybe): implement choosing between attack and defense card slots with
    //  each position incremented by 52 without creating another (identical) enum
    enum AttackCardSlot {
        A(82, 95),
        B(83, 96),
        C(84, 97),
        D(85, 98),
        E(86, 99),
        F(87, 100);

        private final int topPos;
        private final int bottomPos;

        AttackCardSlot(int topPos, int bottomPos) {
            this.topPos = topPos;
            this.bottomPos = bottomPos;
        }

        public int getTopPos() {
            return this.topPos;
        }

        public int getBottomPos() {
            return this.bottomPos;
        }
    }

    enum DefenseCardSlot {
        A(30, 43),
        B(31, 44),
        C(32, 45),
        D(33, 46),
        E(34, 47),
        F(35, 48);

        private final int topPos;
        private final int bottomPos;

        DefenseCardSlot(int topPos, int bottomPos) {
            this.topPos = topPos;
            this.bottomPos = bottomPos;
        }

        public int getTopPos() {
            return this.topPos;
        }

        public int getBottomPos() {
            return this.bottomPos;
        }
    }

    // TODO: to be moved to class LocationCard
    enum LocationCardSlot {
        A(56),
        B(57),
        C(58),
        D(59),
        E(60),
        F(61);

        private final int pos;

        LocationCardSlot(int pos) {
            this.pos = pos;
        }

        public int getPos() {
            return this.pos;
        }
    }

    // Playing card / boss card constructor
    PlayingCard(Board board, String color, char letter, int defaultTopPos, int defaultBottomPos, ImageIcon imageIconTop, ImageIcon imageIconBottom) {
//        super();
        this.BOARD = board;
        this.COLOR = color;
        this.LETTER = letter;
        this.DEFAULT_TOP_POS = defaultTopPos;
        this.DEFAULT_BOTTOM_POS = defaultBottomPos;
        this.IMAGE_ICON_TOP = imageIconTop;
        this.IMAGE_ICON_BOTTOM = imageIconBottom;

        this.currentTopPos = defaultTopPos;
        this.currentBottomPos = defaultBottomPos;
    }

    // TODO: make use of it
    // Location card constructor
//    PlayingCard(char letter, int position, ImageIcon imageIcon) {
//        this(null, "", letter, position, -1, imageIcon, null);
//    }

    // TODO: remove duplicate code within this function
    private void hide(int topPos, int bottomPos) {
        Container contentPane = null;
        switch (this.COLOR) {
            case "blue":
                contentPane = Main.boardBlue.CONTENT_PANE;
                break;
            case "red":
                contentPane = Main.boardRed.CONTENT_PANE;
                break;
            case "noColor":
                contentPane = Main.boardEndTurn.CONTENT_PANE;
                break;
        }
//        JLabel jLabel = (JLabel) Board.J_FRAME.getContentPane().getComponent(topPos);
        if (contentPane == null) throw new AssertionError("contentPane empty: unhandled edge case");
        JLabel jLabel = (JLabel) contentPane.getComponent(topPos);
        jLabel.setIcon(null);
        switch (this.currentStatus) {
            case "attack":
            case "defense":
                jLabel.removeMouseListener(this.mouseAdapterAttDef);
                break;
            case "hand":
                jLabel.removeMouseListener(this.mouseAdapterHand);
                break;
        }
        if (bottomPos != -1) {
            jLabel = (JLabel) contentPane.getComponent(bottomPos);
            jLabel.setIcon(null);
            switch (this.currentStatus) {
                case "attack":
                case "defense":
                    jLabel.removeMouseListener(this.mouseAdapterAttDef);
                    break;
                case "hand":
                    jLabel.removeMouseListener(this.mouseAdapterHand);
                    break;
            }
        }
    }

    // TODO: remove duplicate code within this function
    private void show(int topPos, int bottomPos) {
        Container contentPane = null;
        switch (this.COLOR) {
            case "blue":
                contentPane = Main.boardBlue.CONTENT_PANE;
                break;
            case "red":
                contentPane = Main.boardRed.CONTENT_PANE;
                break;
            case "noColor":
                contentPane = Main.boardEndTurn.CONTENT_PANE;
                break;
        }
        if (contentPane == null) throw new AssertionError("contentPane empty: unhandled edge case");
//        JLabel jLabel = (JLabel) Board.J_FRAME.getContentPane().getComponent(topPos);
        JLabel jLabel = (JLabel) contentPane.getComponent(topPos);
        jLabel.setText("");
        jLabel.setIcon(this.IMAGE_ICON_TOP);
        switch (this.currentStatus) {
            case "attack":
            case "defense":
                jLabel.addMouseListener(this.mouseAdapterHand);
                break;
            case "hand":
                jLabel.addMouseListener(this.mouseAdapterAttDef);
                break;
        }
        if (bottomPos != -1) {
            jLabel = (JLabel) contentPane.getComponent(bottomPos);
            jLabel.setText("");
            jLabel.setIcon(this.IMAGE_ICON_BOTTOM);
            switch (this.currentStatus) {
                case "attack":
                case "defense":
                    jLabel.addMouseListener(this.mouseAdapterHand);
                    break;
                case "hand":
                    jLabel.addMouseListener(this.mouseAdapterAttDef);
                    break;
            }
        }
    }

    // TODO: hide() and show() methods shouldn't need current
    //  positions passed as method parameters, they should request it
    //  themselves
    private void setCurrentPos(int newTopPos, int newBottomPos) {
        this.hide(this.currentTopPos, this.currentBottomPos);
        this.currentTopPos = newTopPos;
        this.currentBottomPos = newBottomPos;
        this.show(this.currentTopPos, this.currentBottomPos);
    }

    private void resetPos() {
        this.setCurrentPos(this.DEFAULT_TOP_POS, this.DEFAULT_BOTTOM_POS);
    }

    public int[] getCurrentPos() {
        return new int[]{this.currentTopPos, this.currentBottomPos};
    }

    public int[] getDefaultPos() {
        return new int[]{this.DEFAULT_TOP_POS, this.DEFAULT_BOTTOM_POS};
    }

    public int[] getAttackPos() {
        for (AttackCardSlot acs : AttackCardSlot.values()) {
            if (String.valueOf(this.LETTER).toUpperCase().equals(acs.name())) {
                return new int[]{acs.getTopPos(), acs.getBottomPos()};
            }
        }
        System.err.println("getAttackPos not found");
        return new int[]{-1, -1};
    }

    public int[] getDefensePos() {
        for (DefenseCardSlot dcs : DefenseCardSlot.values()) {
            if (String.valueOf(this.LETTER).toUpperCase().equals(dcs.name())) {
                return new int[]{dcs.getTopPos(), dcs.getBottomPos()};
            }
        }
        System.err.println("getDefensePos not found");
        return new int[]{-1, -1};
    }

    // TODO: move to the LocationCard class (to be created)
    public int getLocationPos() {
        for (LocationCardSlot lcs : LocationCardSlot.values()) {
            if (String.valueOf(this.LETTER).toUpperCase().equals(lcs.name())) {
                return lcs.getPos();
            }
        }
        System.err.println("getLocationPos not found");
        return -1;
    }

    public String getStatus() {
        return this.currentStatus;
    }

    public void setStatus(String status) {
        this.currentStatus = status;
    }

    public void moveToAttack() {
        this.setCurrentPos(this.getAttackPos()[0], this.getAttackPos()[1]);
        System.out.println("attack cards += 1");
        this.BOARD.setNumOfAttackCards(this.BOARD.getNumOfAttackCards() + 1);
        this.currentStatus = "attack";
    }

    public void moveToDefense() {
        this.setCurrentPos(this.getDefensePos()[0], this.getDefensePos()[1]);
        System.out.println("defense cards += 1");
        this.BOARD.setNumOfDefenseCards(this.BOARD.getNumOfDefenseCards() + 1);
        this.currentStatus = "defense";
    }

    public void moveToHand() {
        this.resetPos();
        switch (this.currentStatus) {
            case "attack":
                this.BOARD.setNumOfAttackCards(this.BOARD.getNumOfAttackCards() - 1);
                System.out.println("attack cards -= 1");
                break;
            case "defense":
                this.BOARD.setNumOfDefenseCards(this.BOARD.getNumOfDefenseCards() - 1);
                System.out.println("defense cards -= 1");
                break;
            case "hand":
                System.out.println("already in hand, no AttDef change");
                break;
        }
        this.currentStatus = "hand";
    }

    public ImageIcon getTopIcon() {
        return this.IMAGE_ICON_TOP;
    }

    public ImageIcon getBottomIcon() {
        return this.IMAGE_ICON_BOTTOM;
    }
}
