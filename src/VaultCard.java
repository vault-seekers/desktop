import javax.swing.ImageIcon;

public class VaultCard extends Card {
    private final Type CARD_TYPE;
    private final ImageIcon ICON;
    private final int POINTS;
    private final String VAULT;
    private Slot pos;

    enum Type {
        COLOR_BLUE("blue"),
        COLOR_GREEN("green"),
        COLOR_RED("red"),
        COLOR_YELLOW("yellow"),
        COLORFUL,
        POINT,
        SECRET_DOCS_BLUE("blue"),
        SECRET_DOCS_GREEN("green"),
        SECRET_DOCS_RED("red"),
        SECRET_DOCS_YELLOW("yellow"),
        THREE_CHOSEN_GEMS("any"),
        HIDDEN_LOOT,
        TRADE;

        private final String color;

        Type() {
            this.color = "";
        }
        Type(String color) {
            this.color = color;
        }

        public String getColor() {
            return this.color;
        }
    }

    enum Slot {
        A(69),
        B(70),
        C(71),
        D(72),
        E(73),
        F(74),
        NONE(-1);

        private final int POS;

        Slot(int pos) {
            this.POS = pos;
        }

        public int getPos() {
            return this.POS;
        }
    }

//    VaultCard(VaultCardType cardType, ImageIcon icon, VaultCardSlot slot, int points, String vault) {
//    VaultCard(Type cardType, Slot slot, int points, String vault) {
    VaultCard(Type cardType, int points, String vault) {
        this.CARD_TYPE = cardType;
        this.POINTS = points;
        this.VAULT = vault;

        this.ICON = createVaultIcon(vault, cardType.getColor(), points);
        this.pos = Slot.NONE;
    }

    public Slot getPos() {
        return this.pos;
    }

    public void setPos(Slot slot) {
        this.pos = slot;
    }

    public ImageIcon getIcon() {
        return this.ICON;
    }

    private ImageIcon createVaultIcon(String vault, String color, int points) {
        if (color.equals("")) {
            color = this.CARD_TYPE.toString().toLowerCase();
        } else if (this.CARD_TYPE.name().startsWith("SECRET_DOCS_")) {
            color = this.CARD_TYPE.getColor() + "_secret_docs";
        }
//        System.out.printf("VaultCard.createVaultIcon(vault: %s, color: %s, points: %d)%n", vault, color, points);
        String path;
        String desc;
        // Image path
        if (points == 0) {
//            vaultStrings[0] = "images/vault" + vault + "_" + color + ".png";
            path = "graphics/cards/vault/vault" + vault + "_" + color + ".png";
        } else {
//            vaultStrings[0] = "images/vault" + vault + "_" + color + "_" + points + ".png";
            path = "graphics/cards/vault/vault" + vault + "_" + color + "_" + points + ".png";
        }

        // Image description
        if (points == 0) {
//            vaultStrings[1] = Board.firstCharToUpper(color) + " vault " + vault + " card";
            desc = Board.firstCharToUpper(color) + " vault " + vault + " card";
        } else {
//            vaultStrings[1] = Board.firstCharToUpper(color) + " " + points + "-point vault " + vault + " card";
            desc = Board.firstCharToUpper(color) + " " + points + "-point vault " + vault + " card";
        }
        return Board.createImageIcon(path, desc);
    }
}
