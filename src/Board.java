import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class Board {
    private static final Color DEFAULT_COLOR = new Color(238, 238, 238);
    private static int numOfVaultOneCardsInDeck = 20;
    private static int numOfVaultTwoCardsInDeck = 22;
    private static int numOfVaultCardsOnBoard;
    public static final JLabel[] vaultCardSlots = new JLabel[6];
    private static VaultCard[] vaultOneCards = new VaultCard[20];
    private static VaultCard[] vaultTwoCards = new VaultCard[22];
    public static int[] attackCardCount = new int[vaultCardSlots.length];
    public static int[] defenseCardCount = new int[vaultCardSlots.length];
//    private static String[][] vaultOneStrings = new String[vaultOneCards.length][2];
//    private static String[][] vaultTwoStrings = new String[vaultTwoCards.length][2];

    private final String COLOR;
    private boolean hasExtraBoss;
    private boolean isHidden = true;
    private int attackCardLimit = 1;
    private int defenseCardLimit = 2;
    private int numOfAttackCards;
    private int numOfDefenseCards;
    private int numOfBlueGems = 0;
    private int numOfGreenGems = 0;
    private int numOfRedGems = 0;
    private int numOfYellowGems = 0;
    private final PlayingCard[] playingCards = new PlayingCard[6];
    public final JLabel[] J_LABELS = new JLabel[156];
    //    public final JPanel[] J_PANELS = new JPanel[156];
    public final Container CONTENT_PANE = new Container();

    // Constructor used for "endTurnBoard" only
    Board() {
        this("noColor");
    }

    // Constructor used for all *player* boards
    Board(String color) {
        this.COLOR = color;
//        Main.mainFrame = jFrame;

        // Set up the content pane.
//        Main.mainFrame.setContentPane(new Container());
        Main.mainFrame.setContentPane(CONTENT_PANE);
        this.initPane(CONTENT_PANE);
        this.placeLocationCards();

        // player board function calls
        if (!this.COLOR.equals("noColor")) {
            // TODO: place this button as soon as the att/def limits are met,
            //  hide the button if the att/def amounts change (e.g. player unselects a selected card)
            //  (do the same with placing the Next Round button)
            this.placeSwapBoardsButton();
            this.placeBossCard();
            this.updateAttackDefenseLabel();
            this.initPlayingCards();
            this.placePlayingCards();
        }

        // Render Main.mainFrame's components
        Main.mainFrame.pack();

        Main.mainFrame.setVisible(true);
        this.hide();
    }

    protected static ImageIcon createImageIcon(String path, String description) {
        Path imgFile = FileSystems.getDefault().getPath(path);
        Image image;
        try {
            image = ImageIO.read(Files.newInputStream(imgFile));
        } catch (IOException e) {
            System.err.println("Couldn't access file: " + path);
            // TODO (maybe): return a missing texture (alternating purple-black texture grids)
            //  and log the path causing this
            return null;
        }
        return new ImageIcon(image, description);
    }

    // TODO: finish implementing
    protected static void resolveConflict() {
        Board[] boards = {Main.boardBlue, Main.boardRed};

        // Counting attack and defense cards
        for (int i = 0; i < 2; i++) {
            final int firstAttackCardTopPos = boards[i].playingCards[0].getAttackPos()[0] + i * 26;
            final int firstDefenseCardTopPos = boards[i].playingCards[0].getDefensePos()[0] - i * 26;
            for (int j = 0; j < vaultCardSlots.length; j++) {
//                JLabel jLabel = (JLabel) Main.mainFrame.getContentPane().getComponent(firstDefenseCardTopPos + j);
                JLabel jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(firstDefenseCardTopPos + j);
                if (jLabel.getIcon() != null) {
                    defenseCardCount[j] += 1;
//                } else {
//                    System.err.println("resolveConflict (attack): getIcon() returned null");
                }

//                jLabel = (JLabel) Main.mainFrame.getContentPane().getComponent(firstAttackCardTopPos + j);
                jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(firstAttackCardTopPos + j);
                if (jLabel.getIcon() != null) {
                    attackCardCount[j] += 1;
                    System.out.printf("       board[i: %d] attackCardCount[j: %d] before: %d, now: %d%n", i, j, attackCardCount[j] - 1, attackCardCount[j]);
                    System.out.println(jLabel.getIcon());
//                } else {
//                    System.err.println("resolveConflict (defense): getIcon() returned null");
                }
            }
        }
        System.out.println(Arrays.toString(attackCardCount));
        System.out.println(Arrays.toString(defenseCardCount));

        // resolve conflict thread
        MyRunnable myRunnable = new MyRunnable();
        Thread thread = new Thread(myRunnable);
        thread.start();
    }

    protected static void revealCards() {
        revealAttackCards();
        revealDefenseCards();
    }

    // TODO: remove duplicate code (revealAttackCards 1/2)
    private static void revealAttackCards() {
        Board[] boards = {Main.boardBlue, Main.boardRed};
        for (int i = 0; i < 2; i++) {
            final int firstCardTopPos = boards[i].playingCards[0].getAttackPos()[0];
            for (int j = 0; j < vaultCardSlots.length; j++) {
                String cardStatus = boards[i].playingCards[j].getStatus();
                if (cardStatus.equals("attack")) {
                    System.out.printf("   board[i: %d] revealAttackCards(): j == %d%n", i, j);
                    JLabel jLabel1 = new JLabel(boards[i].playingCards[j].getTopIcon());
                    Main.boardEndTurn.CONTENT_PANE.remove(firstCardTopPos + j + 26 * i);
                    Main.boardEndTurn.CONTENT_PANE.add(jLabel1, firstCardTopPos + j + 26 * i);

                    JLabel jLabel2 = new JLabel(boards[i].playingCards[j].getBottomIcon());
                    Main.boardEndTurn.CONTENT_PANE.remove(firstCardTopPos + 13 + j + 26 * i);
                    Main.boardEndTurn.CONTENT_PANE.add(jLabel2, firstCardTopPos + 13 + j + 26 * i);
                }
            }
        }
    }

    // TODO: remove duplicate code (revealAttackCards 2/2)
    private static void revealDefenseCards() {
        Board[] boards = {Main.boardBlue, Main.boardRed};
        for (int i = 0; i < 2; i++) {
            final int firstCardTopPos = boards[i].playingCards[0].getDefensePos()[0];
            for (int j = 0; j < vaultCardSlots.length; j++) {
                String cardStatus = boards[i].playingCards[j].getStatus();
                if (cardStatus.equals("defense")) {
                    JLabel jLabel1 = new JLabel(boards[i].playingCards[j].getTopIcon());
                    Main.mainFrame.getContentPane().remove(firstCardTopPos + j - 26 * i);
                    Main.mainFrame.getContentPane().add(jLabel1, firstCardTopPos + j - 26 * i);

                    JLabel jLabel2 = new JLabel(boards[i].playingCards[j].getBottomIcon());
                    Main.mainFrame.getContentPane().remove(firstCardTopPos + 13 + j - 26 * i);
                    Main.mainFrame.getContentPane().add(jLabel2, firstCardTopPos + 13 + j - 26 * i);
                }
            }
        }
    }

    private static int getNumOfVaultCardsOnBoard() {
        return numOfVaultCardsOnBoard;
    }

    private static void setNumOfVaultCardsOnBoard(int count) {
        numOfVaultCardsOnBoard = count;
    }

//    public static void resetPlayingCardsPos() {
//        for (int i = 0; i < Board.vaultCardSlots.length; i++) {
//            resetPlayingCardsPos();
//        }
//    }

    public static void resetPlayingCardsPos(int i) {
        Main.boardBlue.playingCards[i].moveToHand();
        Main.boardRed.playingCards[i].moveToHand();
    }

    public static void hideConflictBoardCards() {
        for (int i = 0; i < vaultCardSlots.length; i++) {
            Main.boardEndTurn.hideCard(i);
        }
    }

    public void hideCard(int pos) {
        for (int i = 0; i < 2; i++) {
            // Hide attack cards' icons.
            JLabel jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.AttackCardSlot.A.getTopPos() + pos + i * 26);
            jLabel.setIcon(null);
            jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.AttackCardSlot.A.getBottomPos() + pos + i * 26);
            jLabel.setIcon(null);

            // Hide defense cards' icons.
            jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.DefenseCardSlot.A.getTopPos() + pos - i * 26);
            jLabel.setIcon(null);
            jLabel = (JLabel) Main.boardEndTurn.CONTENT_PANE.getComponent(PlayingCard.DefenseCardSlot.A.getBottomPos() + pos - i * 26);
            jLabel.setIcon(null);
        }
    }

    public static void initVaultCards() {
        VaultCard.Type[] colors = {
                VaultCard.Type.COLOR_BLUE, VaultCard.Type.COLOR_GREEN,
                VaultCard.Type.COLOR_RED, VaultCard.Type.COLOR_YELLOW};
        String[] vaults = {"1", "12", "2"};

        // Two- to four-point vault 1/12 cards (strings[i][0] path, strings[i][1] description), total 12 cards
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                String vault = "";
                int points = j + 2;
                switch (points) {
                    case 2:
                        vault = vaults[0];
                        break;
                    case 3:
                    case 4:
                        vault = vaults[1];
                        break;
                }
//                addVaultString(vaultOneStrings[i * 3 + j], vault, colors[i], points);
//                ImageIcon icon = createImageIcon(
//                        "images/vault" + vault + "_" + colors[i] + "_" + points + ".png",
//                        Board.firstCharToUpper(colors[i].getColor()) + " " + points + "-point vault " + vault + " card");
                vaultOneCards[i * 3 + j] = new VaultCard(colors[i], points, vault);
            }
        }

        // Three- to five-point vault 12/2 cards (strings[i][0] path, strings[i][1] description), total 12 cards
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                String vault = "";
                int points = j + 3;
                switch (points) {
                    case 3:
                    case 4:
                        vault = vaults[1];
                        break;
                    case 5:
                        vault = vaults[2];
                        break;
                }
//                addVaultString(vaultTwoStrings[i * 3 + j], vault, colors[i], points);
//                ImageIcon icon = createImageIcon(
//                        "images/vault" + vault + "_" + colors[i] + "_" + points + ".png",
//                        Board.firstCharToUpper(colors[i].getColor()) + " " + points + "-point vault " + vault + " card");
                vaultTwoCards[i * 3 + j] = new VaultCard(colors[i], points, vault);
            }
        }

        vaultOneCards[12] = new VaultCard(VaultCard.Type.COLORFUL, 0, vaults[1]);
        vaultOneCards[13] = new VaultCard(VaultCard.Type.COLORFUL, 0, vaults[1]);
        vaultOneCards[14] = new VaultCard(VaultCard.Type.THREE_CHOSEN_GEMS, 3, vaults[0]);
        vaultOneCards[15] = new VaultCard(VaultCard.Type.THREE_CHOSEN_GEMS, 3, vaults[0]);
        vaultOneCards[16] = new VaultCard(VaultCard.Type.HIDDEN_LOOT, 3, vaults[0]);
        vaultOneCards[17] = new VaultCard(VaultCard.Type.HIDDEN_LOOT, 3, vaults[0]);
        vaultOneCards[18] = new VaultCard(VaultCard.Type.POINT, 1, vaults[1]);
        vaultOneCards[19] = new VaultCard(VaultCard.Type.POINT, 1, vaults[1]);

//        addVaultString(vaultOneStrings[12], vaults[1], "colorful", 0);
//        addVaultString(vaultOneStrings[13], vaults[1], "colorful", 0);
//        addVaultString(vaultOneStrings[14], vaults[0], "any", 3);
//        addVaultString(vaultOneStrings[15], vaults[0], "any", 3);
//        addVaultString(vaultOneStrings[16], vaults[0], "hidden", 3);
//        addVaultString(vaultOneStrings[17], vaults[0], "hidden", 3);
//        addVaultString(vaultOneStrings[18], vaults[1], "point", 1);
//        addVaultString(vaultOneStrings[19], vaults[1], "point", 1);

        vaultTwoCards[12] = new VaultCard(VaultCard.Type.COLORFUL, 0, vaults[1]);
        vaultTwoCards[13] = new VaultCard(VaultCard.Type.COLORFUL, 0, vaults[1]);
        vaultTwoCards[14] = new VaultCard(VaultCard.Type.POINT, 1, vaults[1]);
        vaultTwoCards[15] = new VaultCard(VaultCard.Type.POINT, 2, vaults[2]);
        vaultTwoCards[16] = new VaultCard(VaultCard.Type.TRADE, 3, vaults[2]);
        vaultTwoCards[17] = new VaultCard(VaultCard.Type.TRADE, 3, vaults[2]);

//        addVaultString(vaultTwoStrings[12], vaults[1], "colorful", 0);
//        addVaultString(vaultTwoStrings[13], vaults[1], "colorful", 0);
//        addVaultString(vaultTwoStrings[14], vaults[1], "point", 1);
//        addVaultString(vaultTwoStrings[15], vaults[2], "point", 2);
//        addVaultString(vaultTwoStrings[16], vaults[2], "trade", 0);
//        addVaultString(vaultTwoStrings[17], vaults[2], "trade", 0);

        vaultTwoCards[18] = new VaultCard(VaultCard.Type.SECRET_DOCS_BLUE, 0, vaults[2]);
        vaultTwoCards[19] = new VaultCard(VaultCard.Type.SECRET_DOCS_GREEN, 0, vaults[2]);
        vaultTwoCards[20] = new VaultCard(VaultCard.Type.SECRET_DOCS_RED, 0, vaults[2]);
        vaultTwoCards[21] = new VaultCard(VaultCard.Type.SECRET_DOCS_YELLOW, 0, vaults[2]);

//        addVaultString(vaultTwoStrings[18], vaults[2], "blue_secret_docs", 0);
//        addVaultString(vaultTwoStrings[19], vaults[2], "green_secret_docs", 0);
//        addVaultString(vaultTwoStrings[20], vaults[2], "red_secret_docs", 0);
//        addVaultString(vaultTwoStrings[21], vaults[2], "yellow_secret_docs", 0);
    }


    public static void shuffleVaultCards() {
        vaultOneCards = shuffleVaultCards(vaultOneCards);
        vaultTwoCards = shuffleVaultCards(vaultTwoCards);
//        System.out.println("fine shuffleVCs");
    }

    public static VaultCard[] shuffleVaultCards(VaultCard[] cards) {
        int originalLength = cards.length;
        VaultCard[] tmpCards = new VaultCard[originalLength];
        int index = 0;
        while (index < originalLength) {
            int randomIndex = (int) (Math.random() * 100) % cards.length;
//            System.out.println("VC[] shuffleVCs: assigning " + cards[randomIndex]);
            tmpCards[index] = cards[randomIndex];
            cards = excludeArrayElement(cards, randomIndex);
            index++;
        }

        return tmpCards;
    }

    public static String[][] shuffle2DArray(String[][] array) {
        int originalLength = array.length;
        String[][] tmpArray = new String[originalLength][2];
        int index = 0;
//        System.out.println("shuffle2DArray start");
//        System.out.println(Arrays.deepToString(array));
        while (index < originalLength) {
//            System.out.printf("%narray.length: %d%n", originalLength);
            int randomIndex = (int) (Math.random() * 100) % array.length;
//            System.out.printf("currentIndex: %d%nrandomIndex: %d%n", index, randomIndex);
            String[] strings = array[randomIndex];

//            if (Arrays.deepToString(tmpArray).contains(Arrays.toString(strings))) {
//                System.out.println("already contains, skipping");
            /*} else */
            if (Arrays.equals(tmpArray[index], new String[]{null, null})) {
                tmpArray[index] = strings;

                array = excludeArrayElement(array, randomIndex);

//                System.out.println("empty, added");
                index++;
            } else {
                System.out.printf("occupied (%s), skipping%n", Arrays.toString(tmpArray[index]));
            }
        }
//        System.out.printf("%d >= %d%n", index, array.length);
//        System.out.println(tmpArray.length);
//        System.out.println(Arrays.deepToString(tmpArray));

        // clone() should not be necessary here because the tmpArray object isn't used elsewhere and is created only
        // when this shuffling method is executed
        return tmpArray.clone();
    }

    public static VaultCard[] excludeArrayElement(VaultCard[] array, int index) {
//        VaultCard[] tmpArray = array.clone();
        if (Arrays.deepEquals(array, new VaultCard[]{})) {
            System.err.println("excludeArrayElement: input array is empty");
            return array;
        }

        VaultCard[] newArray = new VaultCard[0];
        for (int i = 0; i < array.length; i++) {
            if (i == index) {
                continue;
            }

            newArray = Arrays.copyOf(newArray, newArray.length + 1);
            newArray[newArray.length - 1] = array[i];
        }
        return newArray;
    }

    public static String[][] excludeArrayElement(String[][] array, int index) {
//        String[][] tmpArray = array.clone();
//        if (Arrays.deepEquals(tmpArray, new String[][]{})) {
        if (Arrays.deepEquals(array, new String[][]{})) {
            System.err.println("excludeArrayElement: input array is empty");
//            return tmpArray;
            return array;
        }

        String[][] newArray = new String[0][];
        for (int i = 0; i < array.length; i++) {
            if (i == index) {
                continue;
            }

            newArray = Arrays.copyOf(newArray, newArray.length + 1);
            newArray[newArray.length - 1] = array[i];
        }
        return newArray;
    }

    public static JLabel cloneJLabelIcon(JLabel oldJLabel) {
        JLabel newJLabel = new JLabel();
        newJLabel.setIcon(oldJLabel.getIcon());
        return newJLabel;
    }

    public static int getNumOfVaultCardsInDeck(int vaultNum) {
        if (vaultNum == 1) {
            return numOfVaultOneCardsInDeck;
        } else if (vaultNum == 2) {
            return numOfVaultTwoCardsInDeck;
        }
        System.err.printf("getNumOfVaultOneCardsInDeck(%d) doesn't exist%n", vaultNum);
        return -1;
    }

    public static void setNumOfVaultCardsInDeck(int vaultNum, int count) {
        if (vaultNum == 1) {
            numOfVaultOneCardsInDeck = count;
        } else if (vaultNum == 2) {
            numOfVaultTwoCardsInDeck = count;
        }
    }

    public static String firstCharToUpper(String text) {
        if (text.length() > 0) {
            return text.substring(0, 1).toUpperCase() + text.substring(1);
        } else {
            System.err.println("firstCharToUpper: Input string is of zero length");
            return text;
        }
    }

//
//    public static void placeVaultCards() {
//        for (int i = 0; i < 2; i++) {
//            Main.boardBlue.placeVaultCards(i + 1);
//            Main.boardRed.placeVaultCards(i + 1);
//        }
//    }

    // TODO: implement hiding all ContentPane elements instead of all J_LABELS and J_PANELS
    protected void hide() {
        this.CONTENT_PANE.setVisible(false);
//        for (JLabel jLabel : this.J_LABELS) {
//            jLabel.setVisible(false);
//        }
//        for (JPanel jPanel : this.J_PANELS) {
//            if (jPanel != null) {
//                jPanel.setVisible(false);
//            }
//        }
        this.isHidden = true;
    }

    // TODO: implement showing all ContentPane elements instead of all J_LABELS and J_PANELS
    protected void show() {
        this.CONTENT_PANE.setVisible(true);
//        for (JLabel jLabel : this.J_LABELS) {
//            jLabel.setVisible(true);
//        }
//        for (JPanel jPanel : this.J_PANELS) {
//            if (jPanel != null) {
//                jPanel.setVisible(true);
//            }
//        }
        if (!this.COLOR.equals("noColor")) {
            this.updateAttackDefenseLabel();
        }
        this.isHidden = false;
    }

    protected boolean isHidden() {
        return this.isHidden;
    }

    private int getGemAmount(String color) {
        switch (color) {
            case "blue":
                return this.numOfBlueGems;
            case "green":
                return this.numOfGreenGems;
            case "red":
                return this.numOfRedGems;
            case "yellow":
                return this.numOfYellowGems;
            default:
                System.err.println("getGemAmount error: invalid color");
                return -1;
        }
    }

    // TODO: make use of this
    private void setGemAmount(String color, int amount) {
        if (amount < 0) {
            System.err.println("setGemAmount error: invalid amount");
            return;
        }
        switch (color) {
            case "blue":
                this.numOfBlueGems = amount;
                break;
            case "green":
                this.numOfGreenGems = amount;
                break;
            case "red":
                this.numOfRedGems = amount;
                break;
            case "yellow":
                this.numOfYellowGems = amount;
                break;
            default:
                System.err.println("setGemAmount error: invalid color");
                break;
        }
    }

    // TODO: decrease the amount of gems given from the gem bank
    public static void giveStarterGems() {
        Main.boardRed.giveThreeRandomGems();
        Main.boardBlue.giveThreeRandomGems();
    }

    private void giveThreeRandomGems() {
        for (int i = 0; i < 3; i++) {
            int randomNum = (int) Math.round(Math.random() * 10) % 4;
            switch (randomNum) {
                case 0:
                    this.numOfBlueGems += 1;
                    break;
                case 1:
                    this.numOfGreenGems += 1;
                    break;
                case 2:
                    this.numOfRedGems += 1;
                    break;
                case 3:
                    this.numOfYellowGems += 1;
                    break;
                default:
                    System.err.printf("giveThreeRandomGems error: edge case reached (%d)", randomNum);
                    break;
            }
        }
        this.placeGemLabel();
    }

    private static void resetCardStatus() {
        Board[] boards = {Main.boardBlue, Main.boardRed};
        for (Board board : boards) {
            for (PlayingCard playingCard : board.playingCards) {
                playingCard.setStatus("hand");
            }
        }
    }

    private void setHasExtraBoss(boolean hasExtraBoss) {
        this.hasExtraBoss = hasExtraBoss;
    }

    public boolean hasExtraBoss() {
        return this.hasExtraBoss;
    }

    public void assignExtraBoss() {
        this.J_LABELS[131].setText("");
        this.J_LABELS[131].setIcon(createImageIcon(
                "graphics/cards/playing/card21_green_boss_top.png",
                "Green boss card top (grid 2x1)"));
        this.J_LABELS[144].setText("");
        this.J_LABELS[144].setIcon(createImageIcon(
                "graphics/cards/playing/card21_green_bottom.png",
                "Green card bottom (grid 2x1)"));
        setHasExtraBoss(true);
        this.attackCardLimit = 2;
        this.defenseCardLimit = 1;
        this.updateAttackDefenseLabel();
    }

    public void removeExtraBoss() {
        this.J_LABELS[131].setIcon(null);
        this.J_LABELS[144].setIcon(null);
        setHasExtraBoss(false);
        this.attackCardLimit = 1;
        this.defenseCardLimit = 2;
        System.out.printf((this.COLOR.equals("blue") ? "blue" : "red") + " att/maxAtt: %d/%d%ndef/maxDef: %d/%d%n",
                this.numOfAttackCards, this.attackCardLimit, this.numOfDefenseCards, this.defenseCardLimit);
        this.updateAttackDefenseLabel();
    }

    public String getColor() {
        return this.COLOR;
    }

    public int getNumOfAttackCards() {
        return this.numOfAttackCards;
    }

    public void setNumOfAttackCards(int numOfAttackCards) {
        this.numOfAttackCards = numOfAttackCards;
        this.updateAttackDefenseLabel();
    }

    public int getNumOfDefenseCards() {
        return this.numOfDefenseCards;
    }

    public void setNumOfDefenseCards(int numOfDefenseCards) {
        this.numOfDefenseCards = numOfDefenseCards;
        this.updateAttackDefenseLabel();
    }

    public void updateAttackDefenseLabel() {
        Font font = new Font("Consolas", Font.BOLD, 18);
        Color textColor = new Color(238, 238, 200);
        JLabel jLabel;
        JPanel jPanel = new JPanel();
        jPanel.setBackground(new Color(0, 0, 0));
        jPanel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_END;
        jLabel = new JLabel("Attack:");
        jLabel.setForeground(textColor);
        jLabel.setFont(font);
        jLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        jPanel.add(jLabel, c);

        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        jLabel = new JLabel(String.format(" %d/%d", this.numOfAttackCards, this.attackCardLimit));
        jLabel.setForeground(textColor);
        jLabel.setFont(font);
        jLabel.setHorizontalAlignment(SwingConstants.LEFT);
        jPanel.add(jLabel, c);

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        jLabel = new JLabel("Defense:");
        jLabel.setForeground(textColor);
        jLabel.setFont(font);
        jLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        jPanel.add(jLabel, c);

        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.LINE_START;
        jLabel = new JLabel(String.format(" %d/%d", this.numOfDefenseCards, this.defenseCardLimit));
        jLabel.setForeground(textColor);
        jLabel.setFont(font);
        jLabel.setHorizontalAlignment(SwingConstants.LEFT);
        jPanel.add(jLabel, c);

        if (!this.COLOR.equals("noColor")) {
            this.CONTENT_PANE.remove(133);
            this.CONTENT_PANE.add(jPanel, 133);
        }

        this.CONTENT_PANE.validate();
        this.CONTENT_PANE.repaint();
    }

    public JButton createSwapBoardsButton(String text) {
        JButton swapBoardsButton = new JButton(text);
        swapBoardsButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                boolean allAttackCardsPlaced = numOfAttackCards == attackCardLimit;
                boolean allDefenseCardsPlaced = numOfDefenseCards == defenseCardLimit;
                boolean allCardsPlaced = allAttackCardsPlaced && allDefenseCardsPlaced;
                if (allCardsPlaced) {
                    System.out.println("[Board.createSwapBoardsButton() - instance] allCardsPlaced == true");
                    Main.swapBoards(COLOR);
                }
            }
        });
        return swapBoardsButton;
    }

    public JButton createNextRoundButton(String text) {
        JButton nextRoundButton = new JButton(text);
        nextRoundButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                Main.swapExtraBoss();
                Main.swapBoards(COLOR);

                Board.hideConflictBoardCards();
            }
        });
        return nextRoundButton;
    }

    // Fill the pane with empty JLabels
    public void initPane(Container pane) {
        pane.setLayout(new GridLayout(0, 13));

//        char[] chars = {'a', 'b', 'c', 'd', 'e', 'f'};
//        PlayingCard[] locationCards = new PlayingCard[6];
//        for (int i = 0; i < 6; i++) {
//            char letter = chars[i];
//            ImageIcon icon =
//            locationCards[i] = new PlayingCard(letter, 69 + i, icon);
//        }

        for (int i = 0; i < 156; i++) {
//            this.J_LABELS[i] = new JLabel();
            this.J_LABELS[i] = new JLabel(String.valueOf(i), JLabel.CENTER);
            pane.add(this.J_LABELS[i]);
        }
    }

    public void initPlayingCards() {
        char[] chars = {'a', 'b', 'c', 'd', 'e', 'f'};
        for (int i = 0; i < playingCards.length; i++) {
            char letter = chars[i];
            ImageIcon topIcon = createImageIcon(
                    "graphics/cards/playing/card21_" + this.COLOR + "_" + letter + "_top.png",
                    firstCharToUpper(this.COLOR) + " card top " + letter + " (grid 2x1)");
            ImageIcon bottomIcon = createImageIcon(
                    "graphics/cards/playing/card21_" + this.COLOR + "_bottom.png",
                    firstCharToUpper(this.COLOR) + " card bottom (grid 2x1)");
            playingCards[i] = new PlayingCard(this, this.COLOR, letter, 134 + i, 147 + i, topIcon, bottomIcon);
        }
    }

    public void placePlayingCards() {
        int firstCardTopPos = playingCards[0].getDefaultPos()[0];
        MouseAdapter mouseAdapterHand;
        MouseAdapter mouseAdapterAttDef;
        for (int i = 0; i < playingCards.length; i++) {
            JLabel jLabel1 = new JLabel(playingCards[i].getTopIcon());
            JLabel jLabel2 = new JLabel(playingCards[i].getBottomIcon());

            int finalI = i;
            mouseAdapterHand = new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    super.mouseReleased(e);

                    System.out.println("mAHand clicked");
                    switch (e.getButton()) {
                        case MouseEvent.BUTTON1:
                            if (getNumOfAttackCards() < attackCardLimit) {
                                playingCards[finalI].moveToAttack();
                                System.out.println("moveToAttack\n");
                            }
                            break;
                        case MouseEvent.BUTTON3:
                            if (getNumOfDefenseCards() < defenseCardLimit) {
                                System.out.printf("gNOAttCards: %d%n", getNumOfAttackCards());
                                System.out.printf("gNODefCards: %d%n", getNumOfDefenseCards());
                                playingCards[finalI].moveToDefense();
                                System.out.printf("gNOAttCards: %d%n", getNumOfAttackCards());
                                System.out.printf("gNODefCards: %d%n", getNumOfDefenseCards());
                                System.out.println("moveToDefense\n");
                            }
                            break;
                    }
                }
            };

            mouseAdapterAttDef = new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    super.mouseReleased(e);

                    System.out.println("mAttDef clicked");

                    playingCards[finalI].moveToHand();
                    System.out.println("moveToHand\n");
                }
            };

            playingCards[i].mouseAdapterHand = mouseAdapterHand;
            playingCards[i].mouseAdapterAttDef = mouseAdapterAttDef;

            jLabel1.addMouseListener(mouseAdapterHand);
            jLabel2.addMouseListener(mouseAdapterHand);

            this.CONTENT_PANE.remove(firstCardTopPos + i);
            this.CONTENT_PANE.add(jLabel1, firstCardTopPos + i);

            this.CONTENT_PANE.remove(firstCardTopPos + 13 + i);
            this.CONTENT_PANE.add(jLabel2, firstCardTopPos + 13 + i);
        }
    }

    public void placeSwapBoardsButton() {
        JPanel jPanel = new JPanel();
        jPanel.add(createSwapBoardsButton("End Turn"));
        Main.mainFrame.getContentPane().remove(11);
        Main.mainFrame.getContentPane().add(jPanel, 11);
        System.out.printf("placeSwapBoardsButton(): %s%n", this.COLOR);
    }

    public void placeNextRoundButton() {
//        for (int i = 0; i < 2; i++) {
        JPanel jPanel = new JPanel();
        jPanel.add(createNextRoundButton("Next Round"));
        Main.mainFrame.getContentPane().remove(11);
        Main.mainFrame.getContentPane().add(jPanel, 11);

        Main.mainFrame.getContentPane().validate();
        Main.mainFrame.getContentPane().repaint();

        System.out.printf("placeNextRoundButton(): %s%n", this.COLOR);
//        }
    }

    private static JPanel createVaultLabel(int vaultNum) {
        Font font = new Font("Consolas", Font.BOLD, 18);
        Color textColor = new Color(0, 0, 0);

        JPanel jPanel = new JPanel();
        jPanel.setBackground(DEFAULT_COLOR);
        jPanel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.CENTER;
        JLabel jLabel1 = new JLabel(String.format("Vault %s", vaultNum == 1 ? "I" : "II"));
        jLabel1.setForeground(textColor);
        jLabel1.setFont(font);
        jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
        jPanel.add(jLabel1, c);

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.CENTER;
        JLabel jLabel2 = new JLabel(String.valueOf(Board.getNumOfVaultCardsInDeck(vaultNum)));
        jLabel2.setForeground(textColor);
        jLabel2.setFont(font);
        jLabel2.setHorizontalAlignment(SwingConstants.CENTER);
        jPanel.add(jLabel2, c);
        return jPanel;
    }

    public static void placeVaultLabels() {
        for (int i = 0; i < 2; i++) {
            Main.boardBlue.CONTENT_PANE.remove(i + 66);
            Main.boardBlue.CONTENT_PANE.add(createVaultLabel(i + 1), i + 66);

            Main.boardRed.CONTENT_PANE.remove(i + 66);
            Main.boardRed.CONTENT_PANE.add(createVaultLabel(i + 1), i + 66);
        }
    }

    public static void placeVaultCards() {
        for (int i = 0; i < vaultCardSlots.length; i++) {
            // Check for empty (null) JLabels
            if (vaultCardSlots[i] == null) {
                System.out.printf("vaultCardSlots[%d] == null, assigning card ... ", i);

                if (numOfVaultOneCardsInDeck > 0) {
//                    vaultCardSlots[i] = new JLabel(createImageIcon(vaultOneStrings[i][0], vaultOneStrings[i][1]));

//                    vaultCardSlots[i] = new JLabel(createImageIcon(
//                            vaultOneStrings[vaultOneStrings.length - 1][0],
//                            vaultOneStrings[vaultOneStrings.length - 1][1]));
                    vaultCardSlots[i] = new JLabel(vaultOneCards[vaultOneCards.length - 1].getIcon());

//                    vaultCardSlots[i] = new JLabel(createImageIcon(vaultOneStrings[vaultOneStrings.length - 1][0], vaultOneStrings[vaultOneStrings.length - 1][1]));

//                    for (String[] strings : vaultOneStrings) {
//                        for (String string : strings) {
//                            System.out.println(string);
//                        }
//                    }
                    System.out.println();

//                    System.out.println(Arrays.toString(vaultOneStrings[vaultOneStrings.length - 1]));
//                    System.out.println(Arrays.deepToString(vaultOneStrings));

//                    System.out.println(vaultOneStrings.length);

//                    vaultOneStrings = Arrays.copyOf(vaultOneStrings, vaultOneStrings.length - 1);
                    vaultOneCards = Arrays.copyOf(vaultOneCards, vaultOneCards.length - 1);

//                    System.out.println(vaultOneStrings.length);

//                    System.out.println(Arrays.toString(vaultOneStrings[vaultOneStrings.length - 1]));
//                    System.out.println(Arrays.deepToString(vaultOneStrings));


//                    setNumOfVaultCardsInDeck(1, getNumOfVaultCardsInDeck(1) - 1);
                    setNumOfVaultCardsInDeck(1, vaultOneCards.length);
                    setNumOfVaultCardsOnBoard(getNumOfVaultCardsOnBoard() + 1);
                    placeVaultLabels();
                } else if (numOfVaultTwoCardsInDeck > 0) {
//                    vaultCardSlots[i] = new JLabel(createImageIcon(vaultTwoStrings[i][0], vaultTwoStrings[i][1]));
                    vaultCardSlots[i] = new JLabel(vaultTwoCards[vaultTwoCards.length - 1].getIcon());
                    vaultTwoCards = Arrays.copyOf(vaultTwoCards, vaultTwoCards.length - 1);
//                    setNumOfVaultCardsInDeck(2, getNumOfVaultCardsInDeck(2) - 1);
                    setNumOfVaultCardsInDeck(2, vaultTwoCards.length);
                    setNumOfVaultCardsOnBoard(getNumOfVaultCardsOnBoard() + 1);
                    placeVaultLabels();
                } else {
                    // TODO: implement game-ending process (remove vault cards from the board,
                    //  sum points up, decide if a player won/lost or if it's a draw)
                    System.err.println("noVaultCards error\nGame Over!");
                }

                Main.boardBlue.CONTENT_PANE.remove(i + 69);
                Main.boardBlue.CONTENT_PANE.add(vaultCardSlots[i], i + 69);

                Main.boardRed.CONTENT_PANE.remove(i + 69);
                Main.boardRed.CONTENT_PANE.add(cloneJLabelIcon(vaultCardSlots[i]), i + 69);

                Main.boardEndTurn.CONTENT_PANE.remove(i + 69);
                Main.boardEndTurn.CONTENT_PANE.add(cloneJLabelIcon(vaultCardSlots[i]), i + 69);

                System.out.println("DONE");
            } else {
                if (vaultCardSlots[i].getIcon() == null) {
                    System.err.printf("vaultCardSlots[%d] icon doesn't exist%n", i);
                }/* else {
                    System.err.printf("vaultCardSlots[%d].getIcon() == %s%n", i, vaultCardSlots[i].getIcon());
                }*/
            }
        }
    }

    // TODO: place hidden loot card only when the player gets at least one hidden loot card
    //  OR implement a label showing only the number of hidden loot cards in possession
    public static void placeHiddenLootCard() {
        String[] parts = new String[]{"top", "bottom"};
        for (int i = 0; i < 2; i++) {
            JLabel jLabel = new JLabel(createImageIcon(
                    "graphics/cards/hidden/hidden_back_" + parts[i] + ".png",
                    "Hidden loot card " + parts[i] + " (back cover)"));

            Main.boardBlue.CONTENT_PANE.remove(i * 13 + 130);
            Main.boardBlue.CONTENT_PANE.add(jLabel, i * 13 + 130);

            Main.boardRed.CONTENT_PANE.remove(i * 13 + 130);
            Main.boardRed.CONTENT_PANE.add(cloneJLabelIcon(jLabel), i * 13 + 130);
        }
    }

    public void placeBossCard() {
        String[][] parts = new String[][]{
                {"graphics/cards/playing/card21_" + this.COLOR + "_boss_top.png",
                        firstCharToUpper(this.COLOR) + " boss card top (grid 2x1)"},
                {"graphics/cards/playing/card21_" + this.COLOR + "_bottom.png",
                        firstCharToUpper(this.COLOR) + " card bottom (grid 2x1)"}};
        for (int i = 0; i < 2; i++) {
            JLabel jLabel = new JLabel(createImageIcon(parts[i][0], parts[i][1]));
            Main.mainFrame.getContentPane().remove(i * 13 + 132);
            Main.mainFrame.getContentPane().add(jLabel, i * 13 + 132);
        }
    }

    public void placeLocationCards() {
        char[] chars = {'a', 'b', 'c', 'd', 'e', 'f'};
        for (int i = 0; i < 6; i++) {
            char letter = chars[i];
            JLabel jLabel = new JLabel(createImageIcon(
                    "graphics/cards/location/card21_location_" + letter + ".png",
                    "Location card " + String.valueOf(letter).toUpperCase() + " (grid 2x1)"));
            Main.mainFrame.getContentPane().remove(i + 56);
            Main.mainFrame.getContentPane().add(jLabel, i + 56);
        }
    }

    public void placeGemLabel() {
        JPanel jPanel = new JPanel(new GridLayout(0, 2));
        JLabel jLabel;

        String[] colors = new String[]{"blue", "green", "red", "yellow"};
        String[][] pathAndDesc = new String[colors.length][];
        for (int i = 0; i < colors.length; i++) {
            pathAndDesc[i] = new String[]{
                    "graphics/gems/gem_" + colors[i] + ".png",
                    firstCharToUpper(colors[i]) + " gem icon"};
        }
        GridBagConstraints c;
        for (int i = 0; i < colors.length; i++) {
            for (int j = 0; j < 2; j++) {
                c = new GridBagConstraints();
                c.gridx = j; // Grid column
                c.gridy = i; // Grid row
                c.fill = GridBagConstraints.HORIZONTAL;
                c.anchor = GridBagConstraints.CENTER;
                if (j == 0) {
                    jLabel = new JLabel(createImageIcon(pathAndDesc[i][0], pathAndDesc[i][1]));
                } else {
                    jLabel = new JLabel(Integer.toString(getGemAmount(colors[i])));
                }
//            jLabel.setForeground(textColor);
//            jLabel.setFont(font);
                jLabel.setHorizontalAlignment(SwingConstants.RIGHT);
                jPanel.add(jLabel, c);
            }
        }

        switch (this.COLOR) {
            case "blue":
                Main.boardBlue.CONTENT_PANE.remove(146);
                Main.boardBlue.CONTENT_PANE.add(jPanel, 146);
                break;
            case "red":
                Main.boardRed.CONTENT_PANE.remove(146);
                Main.boardRed.CONTENT_PANE.add(jPanel, 146);
                break;
            default:
                System.err.println("placeGemLabel error: invalid color");
        }
    }
}
